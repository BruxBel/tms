from fiction_page import FictionPage
from fiction_locator import FictionPageLocators


def test_open_books_page(driver):
    page = FictionPage(driver)
    page.open_page()
    page.open_fiction()
    assert driver.\
        find_element(*FictionPageLocators.HEADER_FICTION_PAGE)
