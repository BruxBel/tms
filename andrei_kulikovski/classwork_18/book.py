class PageNumberError(Exception):
    message = "Page number shouldn't be more than 4000"

    def __init__(self):
        super().__init__(self.message)


class YearError(Exception):
    message = "Year shouldn't be over 1980"

    def __init__(self):
        super().__init__(self.message)


class AuthorError(Exception):
    message = "Author name should contain letters only"

    def __init__(self):
        super().__init__(self.message)


class PriceError(Exception):
    message = "Price should be between 100 and 1000"

    def __init__(self):
        super().__init__(self.message)


class Book:

    def __init__(self, page_num, year, author, price):
        self.page_num = self.page_num_validation(page_num)
        self.year = self.year_validation(year)
        self.author = self.author_validation(author)
        self.price = self.price_validation(price)

    @staticmethod
    def page_num_validation(page_num):
        if page_num > 4000:
            raise PageNumberError
        return page_num

    @staticmethod
    def year_validation(year):
        if year < 1980:
            raise YearError
        return year

    @staticmethod
    def author_validation(author):
        if not author.isalpha():
            raise AuthorError
        return author

    @staticmethod
    def price_validation(price):
        if not 1000 >= price >= 100:
            raise PriceError
        return price


book_1 = Book(650, 1980, "aaa", 101)
