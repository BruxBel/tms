import pytest
from selenium import webdriver
from andrei_kulikovski.homework_21.test_web import MAIN_PAGE_URL


@pytest.fixture()
def web():
    driver = webdriver.Chrome(executable_path="D:/QA python/Python/"
                                              "chromedriver")
    yield driver
    driver.quit()


@pytest.fixture()
def link(web):
    return web.get(MAIN_PAGE_URL)
