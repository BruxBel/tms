from pages.main_page import MainPage


def test_main_page(driver):
    page = MainPage(driver)
    page.open_page()
    assert page.get_main_sub().text == "Welcome!"
    assert driver.title == "Oscar - Sandbox"
    assert "/en-gb/" in driver.current_url
