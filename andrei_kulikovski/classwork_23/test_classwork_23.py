import pytest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


URL = "https://jsbin.com/cicenovile/1/edit?html,output"


@pytest.fixture()
def driver():
    driver = webdriver.Chrome(
        executable_path="D:/QA python/Python/chromedriver.exe")
    driver.get(URL)
    yield driver
    driver.quit()


# locators
iframe_1_locator = (By.NAME, "<proxy>")
iframe_2_locator = (By.NAME, "JS Bin Output ")
link_to_click_me = (By.CSS_SELECTOR, "a")


def test_frames_alert(driver):
    WebDriverWait(driver, 10).until(
        EC.presence_of_element_located(iframe_1_locator))
    frame1 = driver.find_element(*iframe_1_locator)
    driver.switch_to.frame(frame1)

    WebDriverWait(driver, 10).until(
        EC.presence_of_element_located(iframe_2_locator))
    frame2 = driver.find_element(*iframe_2_locator)
    driver.switch_to.frame(frame2)

    link = driver.find_element(*link_to_click_me)
    link.click()

    alert = driver.switch_to.alert
    alert.accept()

    driver.switch_to.default_content()
    assert driver.find_element(By.ID, "loginbtn").text == 'Login or Register'
