# Задание 1
# На главной странице перейти по ссылке Checkboxes
# Установить галочку для второго чекбокса и проверить что она установлена
# Для первого чекбокса убрать галочку и протестировать, что ее нет

from selenium.webdriver.common.by import By

CHECKBOXES_MENU_XPATH = '//a[text()="Checkboxes"]'
FIRST_CHECKBOX_XPATH = '//input[@type="checkbox"][1]'
SECOND_CHECKBOX_XPATH = '//input[@type="checkbox"][2]'


def test_checkboxes(open_main_page):
    driver = open_main_page
    driver.find_element(By.XPATH, CHECKBOXES_MENU_XPATH).click()
    first_checkbox = driver.find_element(By.XPATH, FIRST_CHECKBOX_XPATH)
    first_checkbox.click()
    assert first_checkbox.is_selected() is True
    second_checkbox = driver.find_element(By.XPATH, SECOND_CHECKBOX_XPATH)
    second_checkbox.click()
    assert second_checkbox.is_selected() is False
