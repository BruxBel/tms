import time
from functools import wraps


# Task1: Напишите декоратор, который печатает перед и после вызова функции
# слова “Before” and “After”
def add_before_after(func):
    def wrapper():
        print('Before')
        func()
        print('After')
    return wrapper


# Task2: Напишите функцию декоратор, которая добавляет 1 к заданному числу
def add_one(func):
    def wrapper(*args):
        print(func(*args) + 1)
    return wrapper


# Task3: Напишите функцию декоратор, которая переводит полученный текст в
# верхний регистр
def make_uppercase(func):
    def wrapper(arg):
        print(func(arg).upper())
    return wrapper


# Task4: Напишите декоратор func_name, который выводит на экран имя функции
# (func.__name__)
def func_name(func):
    @wraps(func)
    def wrapper(*args):
        print(f'Function name is {func.__name__}')
        return func(*args)
    return wrapper


# Task5: Напишите декоратор change, который делает так, что задекорированная
# функция принимает все свои не именованные аргументы в порядке, обратном тому,
# в котором их передали
def change(func):
    def wrapper(*args, **kwargs):
        return func(*args[::-1], **kwargs)
    return wrapper


# Task6: Напишите декоратор, который вычисляет время работы декорируемой
# функции (timer)
def timer(func):
    @wraps(func)
    def wrapper(*args):
        tmp = time.time()
        func(*args)
        print(f'Time execution of {func.__name__} is', time.time() - tmp)
    return wrapper


# Task9: Напишите декоратор, который проверял бы тип параметров функции,
# конвертировал их если надо и складывал:
def typed(type):
    def inner_function(func):
        def wrapper(*args):
            lst = []
            if type == 'str':
                for i in args:
                    if not isinstance(i, str):
                        i = str(i)
                        lst.append(i)
                    else:
                        lst.append(i)
            elif type == 'int':
                for i in args:
                    if not isinstance(i, int):
                        i = float(i)
                        lst.append(i)
                    else:
                        lst.append(i)
            print(func(*lst))
        return wrapper
    return inner_function
