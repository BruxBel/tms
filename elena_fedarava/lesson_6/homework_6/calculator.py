"""Task3: Реализуйте программу, которая спрашивала у пользователя, какую
операцию он хочет произвести над числами, а затем запрашивает два числа и
выводит результат. Проверка деления на 0.
Пример
Выберите операцию:
    1. Сложение
    2. Вычитание
    3. Умножение
    4. Деление
Введите номер пункта меню:  4
Введите первое число: 10
Введите второе число: 3
Частное: 3, Остаток: 3
"""


def addition(x: int, y: int) -> int:
    """addition for 2 integers"""
    print('Сумма:', x + y)


def subtraction(x: int, y: int) -> int:
    """subtraction for 2 integers"""
    print('Разность:', x - y)


def multiplication(x: int, y: int) -> int:
    """multiplication for 2 integers"""
    print('Произведение:', x * y)


def division(x: int, y: int) -> int:
    """division for 2 integers"""
    if y != 0:
        result = x // y
        print('Частное:', result, 'Остаток:', int((x / y - result) * 10))
    else:
        print('На 0 делить нельзя!')


print('Выберите операцию:', '1. Сложение', '2. Вычитание', '3. Умножение',
      '4. Деление', sep='\n')

selection = int(input('Введите номер пункта меню: '))

if 5 > selection > 0:
    number1 = int(input('Введите первое число: '))
    number2 = int(input('Введите второе число: '))
else:
    print('Нету такого пункта!')
if selection == 1:
    addition(number1, number2)
    print(addition.__annotations__)
elif selection == 2:
    subtraction(number1, number2)
    print(subtraction.__annotations__)
elif selection == 3:
    multiplication(number1, number2)
    print(multiplication.__annotations__)
elif selection == 4:
    division(number1, number2)
    print(division.__annotations__)
