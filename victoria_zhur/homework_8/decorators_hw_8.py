from datetime import datetime
from functools import wraps


"""1. Напишите декоратор, который печатает перед и после вызова функции слова
“Before” and “After”"""


def wrapper_with_words(func):
    """
    This function wraps result of function with words 'Before' and 'After'
    """
    @wraps(func)
    def wrapper(*args):
        print("Before")
        print(func(*args))
        print("After")
    return wrapper


"""2. Напишите функцию декоратор, которая добавляет 1 к заданному числу"""


def incrementation_by_1(func):
    """
    This function implements incrementation by 1
    """
    @wraps(func)
    def wrapper(*args):
        return func(*args) + 1
    return wrapper


"""3. Напишите функцию декоратор, которая переводит полученный текст в
верхний регистр"""


def convert_to_upper_case(func):
    """
    This function converts register of text into upper case
    """
    @wraps(func)
    def wrapper(arg):
        return func(arg.upper())
    return wrapper


"""4. Напишите декоратор func_name, который выводит на экран имя функции
(func.__name__) """


def func_name(func):
    """
    This function prints name of the function
    """
    @wraps(func)
    def wrapper(*args, **kwargs):
        print(func.__name__)
        return func(*args, **kwargs)
    return wrapper


"""5. Напишите декоратор change, который делает так, что задекорированная
функция принимает все свои не именованные аргументы в порядке, обратном тому,
в котором их передали"""


def change(func):
    """
    This function reverses function's arguments
    """
    @wraps(func)
    def wrapper(*args):
        return tuple(reversed(args))
    return wrapper


"""6. Напишите декоратор, который вычисляет время работы декорируемой функции
(timer)"""


def timer(func):
    """
    This function calculates performance of function
    """
    @wraps(func)
    def wrapper(*args):
        start_time = datetime.now()
        func(*args)
        end_time = datetime.now()
        return end_time - start_time
    return wrapper
