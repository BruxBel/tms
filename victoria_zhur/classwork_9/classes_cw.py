"""1. Cоздайте класс работник.
У класса есть конструктор, в который надо передать имя и ЗП.
У класса есть 2 метода: получение имени сотрудника и получение количества
созданных работников. При создании экземпляра класса должен вызываться метод,
который отвечает за увеличение количества работников на 1."""


class Employee:

    employee_counter = 0

    def __init__(self, name, wage):
        self.name = name
        self.wage = wage
        Employee.add_employee()

    def get_employee_name(self):
        return self.name

    @classmethod
    def get_employee_count(cls):
        return cls.employee_counter

    @classmethod
    def add_employee(cls):
        cls.employee_counter += 1


employee_1 = Employee("Vika", 2000)
employee_2 = Employee("Olya", 3000)

print(employee_1.get_employee_name())
print(employee_2.get_employee_name())
print(Employee.get_employee_count())


"""2. Путешествие
Вы идете в путешествие, надо подсчитать сколько у денег у каждого студента.
Класс студен описан следующим образом:
class Student:
    def __init__(self, name, money):
        self.name = name
        self.money = money
Необходимо понять у кого больше всего денег и вернуть его имя. Если у
студентов денег поровну вернуть: “all”.
(P.S. метод подсчета не должен быть частью класса)"""


class Student:

    def __init__(self, name, money):
        self.name = name
        self.money = money


Vika = Student("Vika", 1000)
Katya = Student("Katya", 3000)
Pavel = Student("Pavel", 2000)


def who_is_richer(arg):
    """This function defines which student has more money
    """
    students_money = [i.money for i in arg]
    richer = max(students_money)
    if students_money.count(richer) == len(students_money):
        return "all"
    for student in arg:
        if student.money == richer:
            return student.name


assert who_is_richer([Vika, Katya, Pavel]) == "Katya"
