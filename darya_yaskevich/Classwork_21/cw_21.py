import pytest
from selenium import webdriver
from selenium.webdriver.common.by import By


# Task 1
def find_elements():
    try:
        driver = webdriver.Chrome(
            executable_path='C:\\data\\chromedriver_win32\\chromedriver.exe')
        driver.get("https://www.amazon.com/")

        # logo icon
        driver.find_element(By.ID, "nav-logo-sprites")
        driver.find_element(By.XPATH, '//*[@id="nav-logo-sprites"]')

        # search_field
        driver.find_element(By.CSS_SELECTOR, ".nav-search-field > input")
    finally:
        driver.quit()


# Task 2
link = 'https://the-internet.herokuapp.com/login'


@pytest.fixture
def driver():
    driver = webdriver.Chrome('C:\\data\\chromedriver_win32\\chromedriver.exe')
    yield driver
    driver.quit()


def test_login_valid_data(driver):
    driver.get(link)
    driver.find_element(By.ID, 'username').send_keys('tomsmith')
    driver.find_element(By.ID, 'password').send_keys('SuperSecretPassword!')
    driver.find_element(By.XPATH, '//*[@id="login"]/button').submit()
    message = driver.find_element(By.ID, "flash")
    assert message.text == 'You logged into a secure area!\n×'


def test_login_wrong_username(driver):
    driver.get(link)
    driver.find_element(By.ID, 'username').send_keys('tom')
    driver.find_element(By.ID, 'password').send_keys('SuperSecretPassword!')
    driver.find_element(By.XPATH, '//*[@id="login"]/button').submit()
    message = driver.find_element(By.ID, "flash")
    assert message.text == 'Your username is invalid!\n×'
    page = driver.find_element(By.CSS_SELECTOR, "h2")
    assert page.text == "Login Page"
