# У класса есть 2 метода: получение имени сотрудника и получение количества
# созданных работников. При создании экземпляра должен вызываться метод,
# который отвечает за увеличение количества работников на 1.


class Worker:
    total_workers = 0

    def __init__(self, name, salary):
        self.name = name
        self.salary = salary
        Worker.add_worker()

    def get_name(self):
        return f'Name: {self.name}'

    @classmethod
    def get_total_workers(cls):
        return Worker.total_workers

    @classmethod
    def add_worker(cls):
        Worker.total_workers += 1


worker1 = Worker('Bob', 2000)
worker2 = Worker('Paul', 1000)

print('Number of workers:', Worker.get_total_workers())

# 2. Journey. Необходимо понять, у кого больше всего денег и вернуть его имя.
# Если денег поровну - вернуть "all".
# (P.S. метод подсчета не должен быть частью класса)


class Student:
    students = []

    def __init__(self, name, money):
        self.name = name
        self.money = money
        Student.students.append(self)


def most_money(arg):
    their_money = [student.money for student in arg]
    max_money = max(their_money)
    if their_money.count(max_money) == len(their_money):
        return 'all'
    else:
        for student in arg:
            if student.money == max_money:
                return student.name


student1 = Student('Kostya', 180)
student2 = Student('Kate', 200)
student3 = Student('Max', 150)

print(most_money(Student.students))
