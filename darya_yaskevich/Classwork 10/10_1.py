class Student:

    def __init__(self, name, money):
        self.name = name
        self.money = money

    @property
    def get_name(self):
        return self.name

    @property
    def get_money(self):
        return self.money

    @get_money.setter
    def get_money(self, value):
        self.money = value


def most_money(arg: list):
    """
    This function returns the list of names of students
    who has the most money, in case all students have equal money
    it returns 'all'.
    """
    max_money = 0
    richest = []
    for s in arg:
        if s.get_money > max_money:
            max_money = s.get_money
            richest.clear()
            richest.append(s)
        elif s.get_money == max_money:
            richest.append(s)
    if len(richest) == len(arg):
        return 'all'
    else:
        return [student.get_name for student in richest]


student1 = Student('Kostya', 200)
student2 = Student('Kate', 250)
student3 = Student('Max', 250)

students = [student1, student2, student3]

if __name__ == '__main__':
    assert most_money(students) == ['Kate', 'Max']
