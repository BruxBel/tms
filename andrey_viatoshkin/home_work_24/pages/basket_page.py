from andrey_viatoshkin.home_work_24.pages.base_page import BasePage
from andrey_viatoshkin.home_work_24.pages.basket_page_locators import\
    BasketPageLocators


class BasketPage(BasePage):
    URL = 'http://selenium1py.pythonanywhere.com/en-gb/basket/'

    def __init__(self, driver):
        super().__init__(driver, self.URL)

    def get_empty_message(self):
        return self.driver.find_element(*BasketPageLocators.
                                        EMPTY_BIN_MESSAGE_SELECTOR).text
