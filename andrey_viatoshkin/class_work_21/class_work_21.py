from selenium import webdriver
from selenium.webdriver.common.by import By


def test_find_element():
    driver = webdriver.Chrome()
    url = 'https://www.amazon.com/'
    driver.get(url)
    driver.find_element(By.ID, 'desktop-btf-grid-6')
    driver.find_element(By.XPATH, "//*[text() = 'Headsets']")


def test_valid_login():
    driver = webdriver.Chrome()
    url = 'https://the-internet.herokuapp.com/login'
    driver.get(url)
    username_xpath = '//*[@id="username"]'
    username_input = driver.find_element(By.XPATH, username_xpath)
    username_input.send_keys('tomsmith')
    password_xpath = '//*[@id="password"]'
    password_input = driver.find_element(By.XPATH, password_xpath)
    password_input.send_keys('SuperSecretPassword!')
    login_button_xpath = '//*[@type="submit"]'
    driver.find_element(By.XPATH, login_button_xpath).click()
    success_toast_xpath =\
        "//*[contains(text(),'You logged into a secure area!')]"
    search_toast = driver.find_element(By.XPATH, success_toast_xpath)
    assert search_toast.text == 'You logged into a secure area!\n×'


def test_invalid_login():
    driver = webdriver.Chrome()
    url = 'https://the-internet.herokuapp.com/login'
    driver.get(url)
    username_xpath = '//*[@id="username"]'
    username_input = driver.find_element(By.XPATH, username_xpath)
    username_input.send_keys('tomsmith')
    password_xpath = '//*[@id="password"]'
    password_input = driver.find_element(By.XPATH, password_xpath)
    password_input.send_keys('SuperSecretPasswor')
    login_button_xpath = '//*[@type="submit"]'
    driver.find_element(By.XPATH, login_button_xpath).click()
    error_toast_xpath = "//*[contains(text(),'Your password is invalid!')]"
    error_toast = driver.find_element(By.XPATH, error_toast_xpath)
    assert error_toast.text == 'Your password is invalid!\n×'
