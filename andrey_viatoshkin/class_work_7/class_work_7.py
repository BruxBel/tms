from collections.abc import Iterable
import numpy as np

# 1 Напишите функцию-генератор, которая вычисляет числа фибоначчи


def fib(n: int):
    '''
    Count Fibonacci using generator
    '''
    a = 0
    b = 1
    for i in range(n - 1):
        j = a + b
        a = b
        b = j
    yield j


for k in fib(8):
    print(k)


# 2 Напишите генератор списка который принимает список numbers =
# [34.6, -203.4, 44.9, 68.3, -12.2, 44.6, 12.7] и возвращает новый список
# только с положительными числами
numbers = [34.6, -203.4, 44.9, 68.3, -12.2, 44.6, 12.7]


def positive_numbers(list_numbers: list):
    '''
    Find only positive numbers
    '''
    format_list = [number for number in list_numbers if number > 0]
    return format_list


print(positive_numbers([34.6, -203.4, 44.9, 68.3, -12.2, 44.6, 12.7]))


# 3 Необходимо составить список чисел которые указывают на длину слов в
# строке: sentence = "the quick brown fox jumps over the lazy dog", но
# только если слово не "the".

def count_letters(sentence: str):
    '''
    count word length except 'the' word
    '''
    list_sentence = sentence.split(' ')
    list_sentence_count = [len(i) for i in list_sentence if i != 'the']
    return list_sentence_count


print(count_letters('the quick brown fox jumps over the lazy dog'))


# 1. Петя перешел в другую школу. На уроке физкультуры ему понадобилось
# определить своё место в строю. Помогите ему это сделать. Программа
# получает на вход не возрастающую последовательность натуральных чисел,
# означающих рост каждого человека в строю. После этого вводится
# число X – рост Пети.
# Выведите номер, под которым Петя должен встать в строй. Если в строю есть
# люди с одинаковым ростом,таким же, как у Пети, то он должен встать после
# них.rank([165, 163, 162, 160, 157, 157, 155, 154], 162)  #=> 3

def sort_height(height: int):
    '''
    Find place on a row
    '''
    list_1 = [154, 157, 165, 163, 162, 160, 157, 157, 155, 154]
    list_1.append(height)
    list_1.sort(reverse=True)
    for i in range(len(list_1) - 1, -1, -1):
        if list_1[i] == height:
            print(list_1)
            return i


print(sort_height(162))


# 2. Дан список чисел. Выведите все элементы списка, которые больше
# предыдущего элемента.
# [1, 5, 2, 4, 3]  #=> [5, 4]
# [1, 2, 3, 4, 5] #=> [2, 3, 4, 5]


def prev_element(list_1: list):
    '''
    Find number which is bigger then previous one
    '''
    list_2 = []
    for i in range(len(list_1)):
        if list_1[i] > list_1[i - 1]:
            list_2.append(list_1[i])
    return list_2


print(prev_element([1, 5, 2, 4, 3]))

# 3. Напишите программу, принимающую зубчатый массив любого типа и
# возвращающего его "плоскую" копию.
# List = [1, 2, [3, 4, [5, 6], 7], [8, [9, [10]]]]


def flatten(list: list):
    '''
    Print flat array
    '''
    for element in list:
        if isinstance(element, Iterable) and not isinstance(element,
                                                            (str, bytes)):
            yield from flatten(element)
        else:
            yield element


list_1 = [1, 2, [3, 4, [5, 6], 7], [8, [9, [10]]]]
print(list(flatten(list_1)))


# 4. Напишите функцию, которая принимает на вход одномерный массив и два
# числа - размеры выходной матрицы. На выход программа должна подавать
# матрицу нужного размера, сконструированную из элементов массива.


def create_matrix(arr_list: list, length: int, rows: int):
    '''
    Print array with defined length
    '''
    sub_lists = np.array_split(arr_list, rows)
    count = length
    parent_list = []
    for i in sub_lists:
        # print(list(i))
        count += 1
        parent_list.append(list(i))
    print(parent_list)


create_matrix([1, 2, 3, 4, 5, 6], 2, 3)
