# 1.Validate
# Ваша задача написать программу, принимающее число - номер кредитной карты(
# число может быть четным или не четным). И проверяющей может ли такая карта
# существовать. Предусмотреть защиту от ввода букв, пустой строки и т.д.


def cred_card_check(cred_card: list):
    """
    This function checks if your card is valid or no.
    """
    card_check = cred_card
    null_index = 0
    if len(cred_card) % 2 == 1:
        null_index = card_check.pop(0)
    for i in range(len(cred_card)):
        if i % 2 == 0:
            card_check[i] *= 2
            if card_check[i] > 9:
                card_check[i] -= 9
    card_check.append(null_index)
    if sum(card_check) % 10 != 0:
        print('Invalid card number')
    else:
        print('Valid card number')


cred_card = input('Input your credit card number: ')
while cred_card.isdigit() is False:
    cred_card = input('Input correct credit card number: ')
cred_card = [int(i) for i in cred_card]
cred_card_check(cred_card)


# 2. Подсчет количества букв
# На вход подается строка, например, "cccbba" результат работы программы -
# строка “c3b2a"
# Примеры для проверки работоспособности:
# "cccbba" == "c3b2a"
# "abeehhhhhccced" == "abe2h5c3ed"
# "aaabbceedd" == "a3b2ce2d2"
# "abcde" == "abcde"
# "aaabbdefffff" == "a3b2def5"


def letter_count(second_str: str):
    """
    This function counts the similar letters and shows it in a special way
    """
    a = []
    for i in second_str:
        b = i + str(second_str.count(i))
        if b not in a:
            a.append(b)
    a = ''.join(a)
    return a


second_str = input('Input your string: ')
print(letter_count(second_str))


# 3. Простейший калькулятор v0.1
# Реализуйте программу, которая спрашивала у пользователя, какую операцию он
# хочет произвести над числами, а затем запрашивает два числа и выводит
# результат
# Проверка деления на 0.

def calculator(user_operation: str, number_1: int, number_2: int):
    """
    Simple calculator. Four simple operations between two numbers.
    """
    if user_operation == 'division':
        if number_2 == 0:
            return 'INFINITY'
        return number_1 / number_2
    if user_operation == 'multiplication':
        return number_2 * number_1
    if user_operation == 'addition':
        return number_2 + number_1
    if user_operation == 'subtraction':
        return number_1 - number_2


while True:
    user_operation = input('What kind of operation do you want to do?: ')
    if user_operation in ['division',
                          'multiplication',
                          'addition',
                          'subtraction'
                          ]:
        break
    else:
        print('Wrong kind of operation, choose between division,'
              'multiplication,addition and subtraction')
while True:
    try:
        number_1, number_2 = map(int, input('Input two numbers separated by '
                                            'SPACE: ').split())
        break
    except ValueError:
        print('Wrong data input')
print(calculator(user_operation, number_1, number_2))


# 4. Написать функцию с изменяемым числом входных параметров
# При объявлении функции предусмотреть один позиционный и один именованный
# аргумент, который по умолчанию равен None (в примере это аргумент с именем
# name).
# Также предусмотреть возможность передачи нескольких именованных и
# позиционных аргументов.
# Функция должна возвращать следующее
# result = function(1, 2, 3, name=’test’, surname=’test2’, some=’something’)
# Print(result)
# 🡪 {“mandatory_position_argument”: 1, “additional_position_arguments”: (2, 3),
#      “mandatory_named_argument”: {“name”: “test2”},
#      “additional_named_arguments”:       {“surname”: “test2”, “some”:
#      “something”}}


def func_4(x, *args, name=None, **kwargs):
    """
    Function returns argument's characteristics.
    """
    return {'mandatory_position_argument': x,
            'additional_position_arguments': args,
            'mandatory_named_argument': {'name': name},
            'additional_named_arguments': {**kwargs}
            }


result = func_4(1, 2, 3, name='test', surname='test2', some='something')
print(result)


# 5. Работа с областями видимости

# На уровне модуля создать список из 3-х элементов
# Написать функцию, которая принимает на вход этот список и добавляет в него
# элементы. Функция должна вернуть измененный список.
# При этом исходный список не должен измениться. Пример c функцией которая
# добавляет в список символ “a”:
# My_list = [1, 2, 3]
# Changed_list = change_list(My_list)
# Print(My_list)
# 🡪 [1, 2, 3]
# Print(Changed_list)
# 🡪 [1, 2, 3, “a”]

def list_func(list_a: list):
    """
    This function adds additional value in list_a without changing list_a
    """
    list_b = list_a
    list_b.append('additional value')
    return list_b


list_a = [i for i in range(3)]
print('Initial list: ', list_a)
print('New list: ', list_func(list_a))


# 6. Функция проверяющая тип данных
# Написать функцию которая принимает на фход список из чисел, строк и таплов
# Функция должна вернуть сколько в списке элементов приведенных данных
# print(my_function([1, 2, “a”, (1, 2), “b”])
# 🡪 {“int”: 2, “str”: 2, “tuple”: 1}

def data_type_func(initial_list: list):
    """
    This function takes a list and shows count of types.
    """
    int_q = 0
    str_q = 0
    tuple_q = 0
    for i in range(len(initial_list)):

        if isinstance(initial_list[i], int):
            int_q += 1
        if isinstance(initial_list[i], str):
            str_q += 1
        if isinstance(initial_list[i], tuple):
            tuple_q += 1
    type_dict = {'int': int_q, 'str': str_q, 'tuple': tuple_q}
    return type_dict


initial_list = [1, 2, 'a', (1, 2), 'b']
print(data_type_func(initial_list))

# 7. Написать пример чтобы hash от объекта 1 и 2 были одинаковые, а id разные.

a = 3
b = 3
print(id(a), id(b))
print(hash(a), hash(b))


# 8. написать функцию, которая проверяет есть ли в списке объект, которые
# можно вызвать


def last_func(last_list: list):
    """
    This function checks the list and shows another list of bool values of
    each element to be callable.
    """
    return [callable(i) for i in last_list]


last_list = [1, 2, 3, 'a', True, last_func]
print(last_func(last_list))
