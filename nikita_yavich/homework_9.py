# Библиотека
# Создайте класс book с именем книги, автором, кол-м страниц, ISBN, флагом,
# зарезервирована ли книги или нет. Создайте класс пользователь который
# может брать книгу, возвращать, бронировать. Если другой пользователь
# хочет взять зарезервированную книгу(или которую уже кто-то читает - надо
# ему про это сказать).

class Book:
    def __init__(self, title, author, pages, isbn, is_available, is_reserved):
        self.title = title
        self.author = author
        self.pages = pages
        self.isbn = isbn
        self.is_available = is_available
        self.is_reserved = is_reserved

    def change_to_unavailable(self):
        self.is_available = False

    def change_to_reserved(self):
        self.is_reserved = True
        self.is_available = False

    def change_to_available(self):
        self.is_available = True
        self.is_reserved = False


class Reader:
    def __init__(self, taken, reserved):
        self.taken = taken
        self.reserved = reserved

    def take_a_book(self, arg):
        if arg.is_available is False and arg.is_reserved is False:
            print('Sorry, the book is already taken')
        if arg.is_available and arg.is_reserved is False:
            if self.taken is None:
                arg.change_to_unavailable()
                self.taken = arg
                print(
                    f'There is a book {arg.title}.Try to return it in two'
                    f' weeks')
            else:
                print(
                    'You have to return the previous book to take '
                    'another '
                    'one.')
        if arg.is_reserved:
            if self.reserved == arg:
                if self.taken is None:
                    arg.change_to_unavailable()
                    self.taken = arg
                    self.reserved = None
                    print(
                        f'There is a book {arg.title}.'
                        f'Try to return it in two weeks')
                else:
                    print('You have to return the previous book  to take '
                          'another '
                          'one.')
            else:
                print('Sorry,the book is already reserved.')

    def reserve_a_book(self, arg):
        if arg.is_available:
            arg.change_to_reserved()
            self.reserved = arg
            print(f'The book {arg.title} is reserved for you.')
        elif arg.is_reserved:
            print('Sorry,the book is already reserved.')
        elif arg.is_available is False and arg.is_reserved is False:
            print('Sorry, the book is already taken')

    def return_a_book(self, arg):
        if arg.is_reserved:
            if self.reserved == arg:
                print(
                    f'The book {arg.title} is reserved by you.'
                    f' Would you like to take it?')
            else:
                print('Youve missed something.')

        if arg.is_available:
            print('Youve missed something. That book is maybe not ours.')
        if arg.is_available is False:
            if self.taken == arg:
                arg.change_to_available()
                self.taken = None
                print(f'The book {arg.title} is returned. Hope you '
                      f'appreciated the masterpiece.')
            else:
                print('Youve missed something.')


lolita = Book('Lolita', 'Nabokov', 351, '978-5-9985-0537-9', True, False)
marvelous_summer_of_landlords_kublitsky_and_zablotsky = Book(
    'marvelous_summer_of_landlords_kublitsky_and_zablotsky', 'Vasjuchenka',
    103, '978-985-7180-80-6', True, False)
sapiens = Book('Sapiens', 'Harari', 516, '978-0062316097', True, False)

mikita_yavich = Reader(None, None)
andrey_viatoshkin = Reader(None, None)
mikalai_tarasau = Reader(None, None)


# Банковский вклад
# Создайте класс инвестиция. Который содержит необходимые поля и методы,
# например сумма инвестиция и его срок.
# Пользователь делает инвестиция в размере N рублей сроком на R лет под 10%
# годовых (инвестиция с возможностью ежемесячной капитализации - это означает,
# что проценты прибавляются к сумме инвестиции ежемесячно).
# Написать класс Bank, метод deposit принимает аргументы N и R, и возвращает
# сумму, которая будет на счету пользователя.
# https://myfin.by/wiki/term/kapitalizaciya-procentov

class Investment:
    def __init__(self, invest_money, years):
        self.invest_money = invest_money
        self.years = years

    def count_investment(self):
        """
        This method counts and returns the final sum of money at the end of
        10% annual investment with monthly capitalization.
        """
        money_plus = self.invest_money
        for i in range(self.years * 12):
            money_plus += money_plus * 0.1 / 12
        return money_plus


class Bank(Investment):
    def __init__(self, invest_money, years, other_money):
        super().__init__(invest_money, years)
        self.other_money = other_money
        # super(Bank, self).count_investment()  #тож самое что ниже
        # Investment.count_investment() тож самое что ниже

    def deposit(self):
        """
        This method uses parent method to count and return the final sum
         of money at the end of 10% annual investment with monthly
         capitalization.
        """
        return super().count_investment() + self.other_money

    @staticmethod
    def deposit_calculator(invest_money, years):
        """
        This method counts and returns the final sum of money at the end of
        10% annual investment with monthly capitalization.
        """
        money_plus = invest_money
        for i in range(years * 12):
            money_plus += money_plus * 0.1 / 12
        return money_plus

# mikita = Bank(1000, 3,5000)
# print(Bank.deposit(mikita))
# print(mikita.deposit())
# print(Bank.deposit_calculator(1000,3))
